package codetest.schregardus;

import codetest.schregardus.model.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class EventDispatcherImplTest {

	private EventDispatcher eventDispatcher;

	@Before
	public void setUp() throws Exception {
		eventDispatcher = EventDispatcherImpl.getInstance();
	}

	@SuppressWarnings("unchecked")
	@After
	public void tearDown() throws Exception {

		// Reset the event dispatcher, so we don't need to alter the code to write our tests (although this does require an understanding of the implementation)

		Field eventListenersField = eventDispatcher.getClass().getDeclaredField("eventListeners");
		eventListenersField.setAccessible(true);
		Map<Class, List<EventListener>> eventListeners = (Map<Class, List<EventListener>>) eventListenersField.get(eventDispatcher);
		eventListeners.clear();
		eventListenersField.setAccessible(false);

		Field eventCounterField = eventDispatcher.getClass().getDeclaredField("eventCounter");
		eventCounterField.setAccessible(true);
		EventCounter eventCounter = (EventCounter) eventCounterField.get(eventDispatcher);
		eventCounter.resetEventCounts();
		eventCounterField.setAccessible(false);
	}

	@Test
	public void shouldAddEventListeners() throws Exception {

		EventListener<MyEvent> myEventEventListenerA = new MyEventListener();
		EventListener<MyEvent> myEventEventListenerB = new MyEventListener();
		EventListener<TheirEvent> myEventEventListenerC = new TheirEventListener();

		eventDispatcher.addListener(MyEvent.class, myEventEventListenerA);
		eventDispatcher.addListener(MyEvent.class, myEventEventListenerB);
		eventDispatcher.addListener(TheirEvent.class, myEventEventListenerC);

		eventDispatcher.fireEvent(new MyEvent());

		assertThat(eventDispatcher.getEventFireCount(MyEvent.class), is(2));
		assertThat(eventDispatcher.getEventFireCount(TheirEvent.class), is(0));
	}

	@Test
	public void shouldAddEventListenersAndCheckTearDown() throws Exception {

		// Make sure the tear down method is working correctly, and resetting the counts
		EventListener<MyEvent> myEventEventListenerA = new MyEventListener();
		EventListener<MyEvent> myEventEventListenerB = new MyEventListener();
		EventListener<TheirEvent> myEventEventListenerC = new TheirEventListener();

		eventDispatcher.addListener(MyEvent.class, myEventEventListenerA);
		eventDispatcher.addListener(MyEvent.class, myEventEventListenerB);
		eventDispatcher.addListener(TheirEvent.class, myEventEventListenerC);

		eventDispatcher.fireEvent(new MyEvent());

		assertThat(eventDispatcher.getEventFireCount(MyEvent.class), is(2));
		assertThat(eventDispatcher.getEventFireCount(TheirEvent.class), is(0));
	}

	@Test
	public void shouldRemoveEventListeners() throws Exception {

		EventListener<MyEvent> myEventEventListenerA = new MyEventListener();
		EventListener<MyEvent> myEventEventListenerB = new MyEventListener();
		EventListener<TheirEvent> myEventEventListenerC = new TheirEventListener();

		eventDispatcher.addListener(MyEvent.class, myEventEventListenerA);
		eventDispatcher.addListener(MyEvent.class, myEventEventListenerB);
		eventDispatcher.addListener(TheirEvent.class, myEventEventListenerC);

		eventDispatcher.removeListener(MyEvent.class, myEventEventListenerB);

		eventDispatcher.fireEvent(new MyEvent());
		eventDispatcher.fireEvent(new TheirEvent());

		assertThat(eventDispatcher.getEventFireCount(MyEvent.class), is(1)); // should only fire once, for 1 listeners
		assertThat(eventDispatcher.getEventFireCount(TheirEvent.class), is(1));
	}

	@Test
	public void shouldFireEventsBeforeBeingRemoved() throws Exception {

		final EventListener<SlowEvent> slowEventListenerA = new SlowEventListener();
		final EventListener<SlowEvent> slowEventListenerB = new SlowEventListener();
		final EventListener<SlowEvent> slowEventListenerC = new SlowEventListener();

		eventDispatcher.addListener(SlowEvent.class, slowEventListenerA);
		eventDispatcher.addListener(SlowEvent.class, slowEventListenerB);
		eventDispatcher.addListener(SlowEvent.class, slowEventListenerC);

		ExecutorService executorService = Executors.newCachedThreadPool();

		Future<?> fireEventFuture = executorService.submit(new Runnable() {

			@Override
			public void run() {
				eventDispatcher.fireEvent(new SlowEvent());
			}
		});

		Future<?> removeListenerFuture = executorService.submit(new Runnable() {

			@Override
			public void run() {
				try {
					Thread.sleep(1000L); // Make sure the fire event has started
				} catch (InterruptedException e) {
				}
				eventDispatcher.removeListener(SlowEvent.class, slowEventListenerA);
				eventDispatcher.removeListener(SlowEvent.class, slowEventListenerB);
				eventDispatcher.removeListener(SlowEvent.class, slowEventListenerC);
			}
		});

		// Wait until the threads have returned before continuing
		fireEventFuture.get();
		removeListenerFuture.get();

		// As we started the slow event first, all these should have fired before they were all removed
		assertThat(eventDispatcher.getEventFireCount(SlowEvent.class), is(3));
	}

	@Test
	public void shouldRemoveEventsBeforeFiring() throws Exception {

		final EventListener<SlowEvent> slowEventListenerA = new SlowEventListener();
		final EventListener<SlowEvent> slowEventListenerB = new SlowEventListener();
		final EventListener<SlowEvent> slowEventListenerC = new SlowEventListener();

		eventDispatcher.addListener(SlowEvent.class, slowEventListenerA);
		eventDispatcher.addListener(SlowEvent.class, slowEventListenerB);
		eventDispatcher.addListener(SlowEvent.class, slowEventListenerC);

		ExecutorService executorService = Executors.newCachedThreadPool();

		Future<?> removeListenerFuture = executorService.submit(new Runnable() {

			@Override
			public void run() {
				eventDispatcher.removeListener(SlowEvent.class, slowEventListenerA);
				eventDispatcher.removeListener(SlowEvent.class, slowEventListenerB);
				eventDispatcher.removeListener(SlowEvent.class, slowEventListenerC);
			}
		});

		Future<?> fireEventFuture = executorService.submit(new Runnable() {

			@Override
			public void run() {
				try {
					Thread.sleep(1000L); // Make sure the remove task has a chance to start
				} catch (InterruptedException e) {
				}
				eventDispatcher.fireEvent(new SlowEvent());
			}
		});

		// Wait until the threads have returned before continuing
		removeListenerFuture.get();
		fireEventFuture.get();

		// As we started the slow event first, all these should have fired before they were all removed
		assertThat(eventDispatcher.getEventFireCount(SlowEvent.class), is(0));
	}

	@Test
	public void shouldFireEventAfterRemoveFinished() throws Exception {

		final EventListener<SlowEvent> slowEventListenerA = new SlowEventListener();
		final EventListener<SlowEvent> slowEventListenerB = new SlowEventListener();
		final EventListener<SlowEvent> slowEventListenerC = new SlowEventListener();

		eventDispatcher.addListener(SlowEvent.class, slowEventListenerA);
		eventDispatcher.addListener(SlowEvent.class, slowEventListenerB);
		eventDispatcher.addListener(SlowEvent.class, slowEventListenerC);

		ExecutorService executorService = Executors.newCachedThreadPool();

		Future<?> removeListenerFuture = executorService.submit(new Runnable() {

			@Override
			public void run() {
				eventDispatcher.removeListener(SlowEvent.class, slowEventListenerA);
				eventDispatcher.removeListener(SlowEvent.class, slowEventListenerB);
			}
		});

		Future<?> fireEventFuture = executorService.submit(new Runnable() {

			@Override
			public void run() {
				try {
					Thread.sleep(1000L); // Make sure the remove task has a chance to start
				} catch (InterruptedException e) {
				}
				eventDispatcher.fireEvent(new SlowEvent());
			}
		});

		// Wait until the threads have returned before continuing
		removeListenerFuture.get();
		fireEventFuture.get();

		// The c event should still be fired
		assertThat(eventDispatcher.getEventFireCount(SlowEvent.class), is(1));
	}

	@Test
	public void shouldSynchroniseOnAdd() throws Exception {

		final EventListener<SlowEvent> slowEventListenerA = new SlowEventListener();
		final EventListener<SlowEvent> slowEventListenerB = new SlowEventListener();
		final EventListener<SlowEvent> slowEventListenerC = new SlowEventListener();

		ExecutorService executorService = Executors.newCachedThreadPool();

		Future<?> addListenerFuture = executorService.submit(new Runnable() {

			@Override
			public void run() {
				eventDispatcher.addListener(SlowEvent.class, slowEventListenerA);
				eventDispatcher.addListener(SlowEvent.class, slowEventListenerB);
				eventDispatcher.addListener(SlowEvent.class, slowEventListenerC);
			}
		});

		Future<?> removeListenerFuture = executorService.submit(new Runnable() {

			@Override
			public void run() {
				try {
					Thread.sleep(1000L); // Let the add listeners start first
				} catch (InterruptedException e) {
				}
				eventDispatcher.removeListener(SlowEvent.class, slowEventListenerA);
				eventDispatcher.removeListener(SlowEvent.class, slowEventListenerB);
			}
		});

		// Wait until the threads have returned before continuing
		addListenerFuture.get();
		removeListenerFuture.get();

		eventDispatcher.fireEvent(new SlowEvent()); // just fire events to see how many listeners

		// The c event should still be fired
		assertThat(eventDispatcher.getEventFireCount(SlowEvent.class), is(1));
	}

}