package codetest.schregardus.model;

public class MyEventListener implements EventListener<MyEvent> {

	@Override
	public void handleEvent(MyEvent event) {

		System.out.println("Handling my event");

	}
}
