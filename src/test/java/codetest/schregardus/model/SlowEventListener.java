package codetest.schregardus.model;

public class SlowEventListener implements EventListener<SlowEvent> {

	@Override
	public void handleEvent(SlowEvent event) {

		System.out.println("Starting Handling slow event...");

		try {
			Thread.sleep(500L); // pause the thread, so we can test
		} catch (InterruptedException e) {
			//
		}

		System.out.println("...finished Handling slow event");

	}
}
