package codetest.schregardus.model;

public class TheirEventListener implements EventListener<TheirEvent> {

	@Override
	public void handleEvent(TheirEvent event) {

		System.out.println("Handling their event");

	}
}
