package codetest.schregardus;

import codetest.schregardus.model.MyEvent;
import codetest.schregardus.model.TheirEvent;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class EventCounterTest {

	private EventCounter eventCounter;

	@Before
	public void setUp() throws Exception {
		eventCounter = new EventCounter();
	}

	@After
	public void tearDown() throws Exception {
		eventCounter.resetEventCounts();
	}

	@Test
	public void shouldCountFiredEvents() throws Exception {

		eventCounter.countFiredEvent(MyEvent.class);
		eventCounter.countFiredEvent(MyEvent.class);
		eventCounter.countFiredEvent(MyEvent.class);

		eventCounter.countFiredEvent(TheirEvent.class);
		eventCounter.countFiredEvent(TheirEvent.class);

		assertThat(eventCounter.getFiredEventCount(MyEvent.class), is(3));
		assertThat(eventCounter.getFiredEventCount(TheirEvent.class), is(2));
	}

	@Test
	public void shouldReturn0ForNonFiredEvents() throws Exception {

		eventCounter.countFiredEvent(MyEvent.class);
		eventCounter.countFiredEvent(MyEvent.class);

		assertThat(eventCounter.getFiredEventCount(MyEvent.class), is(2));
		assertThat(eventCounter.getFiredEventCount(TheirEvent.class), is(0));
	}

	@Test
	public void shouldResetCounter() throws Exception {

		eventCounter.countFiredEvent(MyEvent.class);
		eventCounter.countFiredEvent(MyEvent.class);
		eventCounter.countFiredEvent(MyEvent.class);

		eventCounter.countFiredEvent(TheirEvent.class);
		eventCounter.countFiredEvent(TheirEvent.class);

		assertThat(eventCounter.getFiredEventCount(MyEvent.class), is(3));
		assertThat(eventCounter.getFiredEventCount(TheirEvent.class), is(2));

		eventCounter.resetEventCounts();

		assertThat(eventCounter.getFiredEventCount(MyEvent.class), is(0));
		assertThat(eventCounter.getFiredEventCount(TheirEvent.class), is(0));
	}
}