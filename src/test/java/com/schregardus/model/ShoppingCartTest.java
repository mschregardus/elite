package com.schregardus.model;

import com.schregardus.service.TourFactory;
import com.schregardus.service.rules.BulkDealPromotionalRule;
import com.schregardus.service.rules.BulkDiscountPromotionalRule;
import com.schregardus.service.rules.FreeTourPromotionalRule;
import com.schregardus.service.rules.PromotionalRule;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Basic tests to prove the use cases provided in the question
 */
public class ShoppingCartTest {

	private ShoppingCart shoppingCart;

	@Before
	public void setUp() throws Exception {

		List<PromotionalRule> promotionalRules = new ArrayList<>();

		promotionalRules.add(new BulkDealPromotionalRule());
		promotionalRules.add(new FreeTourPromotionalRule());
		promotionalRules.add(new BulkDiscountPromotionalRule());

		shoppingCart = new ShoppingCart(promotionalRules);
	}

	@Test
	public void testSampleUseCase1() throws Exception {

		// OH, OH, OH, BC 710.00
		TourFactory tourFactory = TourFactory.getInstance();

		shoppingCart.add(tourFactory.getTour("OH"));
		shoppingCart.add(tourFactory.getTour("OH"));
		shoppingCart.add(tourFactory.getTour("OH"));
		shoppingCart.add(tourFactory.getTour("BC"));

		BigDecimal total = shoppingCart.total();
		assertThat(total, is(new BigDecimal("710.00")));

		List<Tour> tours = shoppingCart.getTours();
		assertThat(tours, notNullValue());
		assertThat(tours.size(), is(7));
		assertThat(tours.get(0), is(tourFactory.getTour("OH")));
		assertThat(tours.get(1), is(tourFactory.getTour("SK", BigDecimal.ZERO)));
		assertThat(tours.get(2), is(tourFactory.getTour("OH")));
		assertThat(tours.get(3), is(tourFactory.getTour("SK", BigDecimal.ZERO)));
		assertThat(tours.get(4), is(tourFactory.getTour("OH")));
		assertThat(tours.get(5), is(tourFactory.getTour("SK", BigDecimal.ZERO)));
		assertThat(tours.get(6), is(tourFactory.getTour("BC")));
	}

	@Test
	public void testSampleUseCase2() throws Exception {

		// OH, SK 300.00
		TourFactory tourFactory = TourFactory.getInstance();

		shoppingCart.add(tourFactory.getTour("OH"));

		BigDecimal total = shoppingCart.total();
		assertThat(total, is(new BigDecimal("300.00")));

		List<Tour> tours = shoppingCart.getTours();
		assertThat(tours, notNullValue());
		assertThat(tours.size(), is(2));
		assertThat(tours.get(0), is(tourFactory.getTour("OH")));
		assertThat(tours.get(1), is(tourFactory.getTour("SK", BigDecimal.ZERO))); // our free sky tower
	}

	@Test
	public void testSampleUseCase3() throws Exception {

		// BC, BC, BC, BC, BC, OH 750.00
		TourFactory tourFactory = TourFactory.getInstance();

		shoppingCart.add(tourFactory.getTour("BC"));
		shoppingCart.add(tourFactory.getTour("BC"));
		shoppingCart.add(tourFactory.getTour("BC"));
		shoppingCart.add(tourFactory.getTour("BC"));
		shoppingCart.add(tourFactory.getTour("BC"));
		shoppingCart.add(tourFactory.getTour("OH"));

		BigDecimal total = shoppingCart.total();
		assertThat(total, is(new BigDecimal("750.00")));

		List<Tour> tours = shoppingCart.getTours();
		assertThat(tours, notNullValue());
		assertThat(tours.size(), is(7));

		// As there are more than 4, the rate for each bridge climb will have a $20 discount
		BigDecimal discountedPrice = tourFactory.getTour("BC").getPrice().add(new BigDecimal("20.00").negate());

		assertThat(tours.get(0), is(tourFactory.getTour("BC", discountedPrice)));
		assertThat(tours.get(1), is(tourFactory.getTour("BC", discountedPrice)));
		assertThat(tours.get(2), is(tourFactory.getTour("BC", discountedPrice)));
		assertThat(tours.get(3), is(tourFactory.getTour("BC", discountedPrice)));
		assertThat(tours.get(4), is(tourFactory.getTour("BC", discountedPrice)));
		assertThat(tours.get(5), is(tourFactory.getTour("OH")));
		assertThat(tours.get(6), is(tourFactory.getTour("SK", BigDecimal.ZERO))); // added by the opera house (and hence free)
	}
}