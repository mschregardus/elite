package codetest.schregardus;

import codetest.schregardus.model.Event;

import java.util.HashMap;
import java.util.Map;

public class EventCounter {

	private Map<Class, Integer> eventFireCount;

	public EventCounter() {
		this.eventFireCount = new HashMap<>();
	}

	public void countFiredEvent(Class<? extends Event> eventClass) {

		Integer integer = eventFireCount.get(eventClass);
		integer = integer == null ? 1 : ++integer;
		eventFireCount.put(eventClass, integer);
	}

	public int getFiredEventCount(Class<? extends Event> eventClass) {
		Integer integer = eventFireCount.get(eventClass);
		return integer == null ? 0 : integer;
	}

	public void resetEventCounts() {
		eventFireCount.clear();
	}
}
