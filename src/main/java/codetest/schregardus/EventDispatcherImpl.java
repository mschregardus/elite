package codetest.schregardus;

import codetest.schregardus.model.Event;
import codetest.schregardus.model.EventListener;

import java.util.*;

public class EventDispatcherImpl implements EventDispatcher {

	private static EventDispatcherImpl ourInstance = new EventDispatcherImpl();

	public static EventDispatcherImpl getInstance() {
		return ourInstance;
	}

	private final Map<Class, List<EventListener>> eventListeners;
	private EventCounter eventCounter;

	private EventDispatcherImpl() {
		// Use a synchronised map, and block on that, rather than an external object
		eventListeners = Collections.synchronizedMap(new HashMap<Class, List<EventListener>>());
		eventCounter = new EventCounter();
	}

	@Override
	public <E extends Event> void addListener(Class<E> eventClass, EventListener<E> listener) {

		synchronized (eventListeners) {
			if (eventListeners.get(eventClass) == null) {
				eventListeners.put(eventClass, new ArrayList<EventListener>());
			}
			eventListeners.get(eventClass).add(listener);
			System.out.printf("Added Listener for class: %s\n", eventClass);
		}
	}

	@Override
	public <E extends Event> void removeListener(Class<E> eventClass, EventListener<E> listener) {

		synchronized (eventListeners) {
			if (eventListeners.keySet().contains(eventClass)) {

				List<EventListener> eventListeners = this.eventListeners.get(eventClass);
				if (eventListeners.contains(listener)) {
					eventListeners.remove(listener);
					System.out.printf("Removed Listener for class: %s\n", eventClass);
				}
			}
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void fireEvent(Event event) {

		if (event != null) {
			synchronized (eventListeners) {
				for (Class eventClass : eventListeners.keySet()) {

					if (event.getClass().equals(eventClass)) {

						List<EventListener> eventListeners = this.eventListeners.get(eventClass);
						if (eventListeners != null && eventListeners.size() > 0) {
							for (EventListener eventListener : eventListeners) {
								eventListener.handleEvent(event);
								eventCounter.countFiredEvent(eventClass);
								System.out.printf("Fired event for class: %s\n", eventClass);
							}
						}
					}
				}
			}
		}
	}

	@Override
	public int getEventFireCount(Class<? extends Event> eventClass) {
		return eventCounter.getFiredEventCount(eventClass);
	}
}
