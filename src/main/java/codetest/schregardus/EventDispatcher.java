package codetest.schregardus;

import codetest.schregardus.model.Event;
import codetest.schregardus.model.EventListener;

public interface EventDispatcher {

	<E extends Event> void addListener(Class<E> eventClass, EventListener<E> listener);

	<E extends Event> void removeListener(Class<E> eventClass, EventListener<E> listener);

	void fireEvent(Event event);

	int getEventFireCount(Class<? extends Event> eventClass);

}
