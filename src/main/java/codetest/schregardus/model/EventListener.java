package codetest.schregardus.model;

public interface EventListener<E extends Event> {

	void handleEvent(E event);
}
