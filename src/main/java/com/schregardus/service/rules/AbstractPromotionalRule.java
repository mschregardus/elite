package com.schregardus.service.rules;

import com.schregardus.model.Tour;

import java.math.BigDecimal;
import java.util.List;

/**
 * Base rule, that handles the plumbing for the chain of command
 */
public abstract class AbstractPromotionalRule implements PromotionalRule {

	private PromotionalRule nextPromotionalRule;
	private BigDecimal promotionalPrice;
	private List<Tour> tours;

	@Override
	public void addPromotionalRule(PromotionalRule promotionalRule) {
		nextPromotionalRule = promotionalRule;
	}

	@Override
	public void sendToNextPromotionalRule(BigDecimal initialPrice, List<Tour> tours) {

		promotionalPrice = initialPrice;
		this.tours = tours;

		// pass on to the next rule if we have one
		if (nextPromotionalRule != null) {
			nextPromotionalRule.sendToNextPromotionalRule(initialPrice, tours);
		}
	}

	@Override
	public BigDecimal getPromotionalPrice() {
		return nextPromotionalRule != null ? nextPromotionalRule.getPromotionalPrice() : promotionalPrice;
	}

	@Override
	public List<Tour> getTours() {
		return nextPromotionalRule != null ? nextPromotionalRule.getTours() : tours;
	}

}
