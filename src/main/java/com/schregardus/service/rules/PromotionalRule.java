package com.schregardus.service.rules;

import com.schregardus.model.Tour;

import java.math.BigDecimal;
import java.util.List;

/**
 * Use the chain of command pattern to process the promotional rules
 */
public interface PromotionalRule {

	/**
	 * Add the next promotional rule in the chain of command
	 *
	 * @param promotionalRule next rule
	 */
	void addPromotionalRule(PromotionalRule promotionalRule);

	/**
	 * Process this rule, then send on to the next rule
	 *
	 * @param initialPrice the starting price for this rule
	 * @param tours the tours to be used to provide the promotional price
	 */
	void sendToNextPromotionalRule(BigDecimal initialPrice,
			List<Tour> tours);

	/**
	 * The promotional price after the rule has been applied
	 *
	 * @return the promotional price
	 */
	BigDecimal getPromotionalPrice();

	/**
	 * All the tours (as rules can affect the total number of tours
	 *
	 * @return all the tours
	 */
	List<Tour> getTours();

}
