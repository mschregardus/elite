package com.schregardus.service.rules;

import com.schregardus.model.Tour;
import com.schregardus.service.TourFactory;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Handles the rule
 *
 * "The Sydney Bridge Climb will have a bulk discount applied, where the price will drop $20, if someone buys more than 4"
 */
public class BulkDiscountPromotionalRule extends AbstractPromotionalRule {

	@Override
	public void sendToNextPromotionalRule(BigDecimal initialPrice, List<Tour> tours) {

		// See if we're eligible
		int bcCounter = 0;
		for (Tour tour : tours) {
			if ("BC".equals(tour.getId())) {
				bcCounter++;
			}
		}

		// And if we are, update the price with the discount, as well as the bridge climb tour objects
		if (bcCounter > 4) {

			List<Tour> tourCopy = new ArrayList<>();
			BigDecimal discountedPrice = new BigDecimal(initialPrice.toString());

			Tour originalTour = TourFactory.getInstance().getTour("BC");
			BigDecimal discount = new BigDecimal("20.00").negate();
			BigDecimal newTourPrice = originalTour.getPrice().add(discount);

			for (Tour tour : tours) {
				if ("BC".equals(tour.getId())) {
					tourCopy.add(TourFactory.getInstance().getTour("BC", newTourPrice));
					discountedPrice = discountedPrice.add(discount);
				} else {
					tourCopy.add(tour);
				}
			}

			super.sendToNextPromotionalRule(discountedPrice, tourCopy);

		} else {
			super.sendToNextPromotionalRule(initialPrice, tours);
		}
	}

}
