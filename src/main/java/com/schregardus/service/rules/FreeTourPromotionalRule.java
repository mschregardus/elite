package com.schregardus.service.rules;

import com.schregardus.model.Tour;
import com.schregardus.service.TourFactory;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Handles the rule "We are going to give a free Sky Tower tour for with every Opera House tour sold"
 * Could be configured for the specific type of free tour
 */
public class FreeTourPromotionalRule extends AbstractPromotionalRule {

	@Override
	public void sendToNextPromotionalRule(BigDecimal initialPrice, List<Tour> tours) {

		List<Tour> tourCopy = new ArrayList<>();

		TourFactory tourFactory = TourFactory.getInstance();
		for (Tour tour : tours) {

			tourCopy.add(tour);

			if ("OH".equals(tour.getId())) {
				tourCopy.add(tourFactory.getTour("SK", BigDecimal.ZERO));
			}
		}

		super.sendToNextPromotionalRule(initialPrice, tourCopy);
	}

}
