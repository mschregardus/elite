package com.schregardus.service.rules;

import com.schregardus.model.Tour;

import java.math.BigDecimal;
import java.util.List;

/**
 * Rule used to calculate the initial price (i.e. the sender in the chain of command pattern)
 */
public class NoPromotionRule extends AbstractPromotionalRule {

	@Override
	public void sendToNextPromotionalRule(BigDecimal initialPrice, List<Tour> tours) {

		BigDecimal total = initialPrice == null ? BigDecimal.ZERO : initialPrice;
		for (Tour tour : tours) {
			total = total.add(tour.getPrice());
		}
		super.sendToNextPromotionalRule(total, tours);
	}
}
