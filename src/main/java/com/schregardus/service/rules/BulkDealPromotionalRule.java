package com.schregardus.service.rules;

import com.schregardus.model.Tour;
import com.schregardus.service.TourFactory;

import java.math.BigDecimal;
import java.util.List;

/**
 * Handles the rule
 *
 * "We are going to have a 3 for 2 deal on opera house ticket. For example, if you buy 3 tickets, you will pay the
 * price of 2 only getting another one completely free of charge."
 */
public class BulkDealPromotionalRule extends AbstractPromotionalRule {

	@Override
	public void sendToNextPromotionalRule(BigDecimal initialPrice, List<Tour> tours) {

		// 3 for 2 of OH
		int ohCounter = 0;
		for (Tour tour : tours) {
			if ("OH".equals(tour.getId())) {
				ohCounter++;
			}
		}

		BigDecimal amountToDiscount = TourFactory.getInstance().getTour("OH").getPrice();

		int noToRemove = ohCounter / 3;
		BigDecimal discount = amountToDiscount.multiply(new BigDecimal("" + noToRemove));

		BigDecimal discountedPrice = initialPrice.add(discount.negate());

		super.sendToNextPromotionalRule(discountedPrice, tours);
	}
}
