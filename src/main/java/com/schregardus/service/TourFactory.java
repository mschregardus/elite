package com.schregardus.service;

import com.schregardus.model.Tour;

import java.math.BigDecimal;

/**
 * Basic factory to create our immutable tours - with option to change the price if required (by the rules)
 *
 * OH Opera house tour $300.00
 * BC Sydney Bridge Climb $110.00
 * SK Sydney Sky Tower $30.00
 */
public class TourFactory {

	private static TourFactory ourInstance = new TourFactory();

	public static TourFactory getInstance() {
		return ourInstance;
	}

	public Tour getTour(String id) {
		return getTour(id, null);
	}

	public Tour getTour(String id, BigDecimal price) {

		if ("OH".equals(id)) {
			return new Tour("OH", "Opera house tour", price == null ? new BigDecimal("300.00") : price);
		} else if ("BC".equals(id)) {
			return new Tour("BC", "Sydney Bridge Climb", price == null? new BigDecimal("110.00"): price);
		} else if ("SK".equals(id)) {
			return new Tour("SK", "Sydney Sky Tower", price == null ? new BigDecimal("30.00") : price);
		}

		throw new IllegalArgumentException(String.format("The id: %s is not a valid tour id", id));
	}

}
