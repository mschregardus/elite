package com.schregardus.service;

import com.schregardus.model.Tour;
import com.schregardus.service.rules.NoPromotionRule;
import com.schregardus.service.rules.PromotionalRule;

import java.math.BigDecimal;
import java.util.List;

/**
 * Based on a set of promotional rules, will calculate the price for a given set of tours, and also the total number of tours
 */
public class PromotionalRulesEngine {

	private PromotionalRule promotionalRule;

	public PromotionalRulesEngine(List<PromotionalRule> promotionalRules) {
		promotionalRule = getRule(promotionalRules);
	}

	public BigDecimal calculatePrice(List<Tour> tours) {

		promotionalRule.sendToNextPromotionalRule(null, tours);
		return promotionalRule.getPromotionalPrice();
	}

	public List<Tour> calculateTours(List<Tour> tours) {

		promotionalRule.sendToNextPromotionalRule(null, tours);
		return promotionalRule.getTours();
	}

	private PromotionalRule getRule(List<PromotionalRule> promotionalRules) {

		PromotionalRule noPromotionRule = new NoPromotionRule(); // The sender rule - works out the initial price

		if (promotionalRules != null && promotionalRules.size() > 0) {

			PromotionalRule lastPromotionalRule = null;
			for (PromotionalRule promotionalRule : promotionalRules) {

				if (lastPromotionalRule == null) {
					lastPromotionalRule = noPromotionRule;
				}

				lastPromotionalRule.addPromotionalRule(promotionalRule);

				lastPromotionalRule = promotionalRule;
			}
		}

		return noPromotionRule;
	}


}
