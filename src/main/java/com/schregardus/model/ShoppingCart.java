package com.schregardus.model;

import com.schregardus.service.PromotionalRulesEngine;
import com.schregardus.service.rules.PromotionalRule;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Shopping cart, as per the question. The updated tours, as well as the newly calculated price is also included. As the cart takes the promotial rules
 * as a parmater, the rules can change on a cart by cart basis - so the rules engine is initialised accordingly for each cart
 */
public class ShoppingCart {

	private PromotionalRulesEngine promotionalRulesEngine;
	private List<Tour> tours;

	public ShoppingCart(List<PromotionalRule> promotionalRules) {
		promotionalRulesEngine = new PromotionalRulesEngine(promotionalRules);
		this.tours = new ArrayList<>();
	}

	public void add(Tour tour) {
		this.tours.add(tour);
	}

	public List<Tour> getTours() {
		return promotionalRulesEngine.calculateTours(tours);
	}

	public BigDecimal total() {
		return promotionalRulesEngine.calculatePrice(tours);
	}
}
