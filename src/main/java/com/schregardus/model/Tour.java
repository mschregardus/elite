package com.schregardus.model;

import java.math.BigDecimal;

/**
 * Immutable tour object, containing basic tour info
 */
public class Tour {

	private final String id;
	private final String name;
	private final BigDecimal price;

	public Tour(String id, String name, BigDecimal price) {
		this.id = id;
		this.name = name;
		this.price = price;
	}

	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public BigDecimal getPrice() {
		return price;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		Tour tour = (Tour) o;

		if (id != null ? !id.equals(tour.id) : tour.id != null)
			return false;
		if (name != null ? !name.equals(tour.name) : tour.name != null)
			return false;
		return !(price != null ? !price.equals(tour.price) : tour.price != null);

	}

	@Override
	public int hashCode() {
		int result = id != null ? id.hashCode() : 0;
		result = 31 * result + (name != null ? name.hashCode() : 0);
		result = 31 * result + (price != null ? price.hashCode() : 0);
		return result;
	}
}
