# Elite Processing Rules

## Overview

The processing rules use the Chain of Command pattern to affect any discount applied to the price, as well as any additional tours that
may be added to the cart.

The `TourFactory` is a convenience class to help create the required tours for testing - this data would normally be stored in a database.

The `PromotionalRulesEngine` processes the original tours, and works out any discount and/or whether to add any additional tours to the 
`ShoppingCart`. I would normally keep the rules engine separate from the model (cart), but the original api specified that the total could
be obtained from the cart directly - and that the rules were passed in the constructor - hence the current design. 
I have also added the the ability to view the final list of tours from the cart.

The rules themselves could be configured for different tour types if required (and that configuration loaded from a database).

I would normally provide more test coverage (i.e. a test for every object), but currently I've just included a test for the `ShoppingCart` to 
demonstrate the basic functionality.
